import { AnimatedObject } from './AnimatedObject';
import { OBJECT_TYPE } from '../constants';


export class Bonus extends AnimatedObject {

    public amount: number;

    constructor(data: IBonusData) {
        super(data, OBJECT_TYPE.BONUS);
        this.amount = data.amount;
    }

}
